# SIAMES ontology generator

Reads a CSV file and save OWL ontology. Set the `SIAMES_SOURCE` environment var with the CSV location and then run


```
pip install -r requirements
python siames-onto-gen/generator.py
```