import pandas
from owlready2 import *

URL = "http://siames.social/"
NAME = "siames.owl"
FILE_NAME = "out/siames"
SOURCE = os.environ['SIAMES_SOURCE']


def run(url=URL, name=NAME, source=SOURCE, file_name=FILE_NAME):
    onto = generate_ontology_definition(url, name)
    fill_individuals(onto, source)
    save_ontology(onto, file_name)


def generate_ontology_definition(url=URL, name=NAME):
    onto = get_ontology(url + name)
    sustainability_onto = get_ontology("file://base_ontologies/sustainability.owl").load()
    energy_onto = get_ontology('http://energy.linkeddata.es/em-kpi/ontology').load()
    onto.imported_ontologies.append(sustainability_onto)
    onto.imported_ontologies.append(energy_onto)
    with onto:
        class has_literal(DataProperty):
            range = [str]

        class Case(sustainability_onto.Approaches):
            pass

        class Indicator(sustainability_onto.Indicator):
            pass

        class Proxy(Thing):
            pass

        class Outcome(Thing):
            pass

        class Sector(Thing):
            pass

        class SubOutcome(Thing):
            pass

        class Source(Thing):
            pass

        class Stakeholder(sustainability_onto.stakeholder, energy_onto.Stakeholder):
            pass

        class SocialDimension(sustainability_onto.social_dimension):
            pass

        class EconomicDimension(sustainability_onto.economic_dimension):
            pass

        class EnvironmentalDimension(sustainability_onto.environmental_dimension):
            pass

        class has_dimension(Indicator >> sustainability_onto.Basic_dimensions):
            pass

        class main_dimension(Case >> sustainability_onto.Basic_dimensions):
            pass

        class has_outcome(Sector >> Outcome):
            pass

        class has_indicator(Outcome >> Indicator):
            domain = [Outcome, SubOutcome]
            range = [Indicator]

        class has_outcome(Case >> Outcome):
            pass

        class has_sector(Case >> Sector):
            pass

        class has_proxy(Indicator >> Proxy):
            pass

        class has_receiver(Indicator >> Stakeholder):
            pass

        class has_suboutcome(Outcome >> SubOutcome):
            pass

        class has_source(ObjectProperty):
            domain = [Indicator, Proxy]
            range = [Source]

        class is_related_to_metric(Indicator >> Indicator):
            pass

        return onto


def fill_individuals(onto, source=SOURCE):
    with onto:
        data = pandas.read_csv(source)
        for index, row in data.iterrows():
            sectors = row.get('Sector').split(',')
            outcome = onto.Outcome(preprocess(row.get('Outcome')))
            outcome.has_literal = [preprocess(row.get('Outcome'), True)]
            project = onto.Case(preprocess(row.get('Project')))
            project.has_literal = [preprocess(row.get('Project'), True)]
            project.has_outcome.append(outcome)

            if row.get('ProjectDimension') is not None:
                class_ = getattr(onto, row.get('ProjectDimension'))
                project_dimension = class_(preprocess(row.get('ProjectDimension')))
                project.main_dimension.append(project_dimension)

            for sector in sectors:
                onto_sector = onto.Sector(preprocess(sector))
                onto_sector.has_literal = [preprocess(sector, True)]
                onto_sector.has_outcome.append(outcome)
                project.has_sector.append(onto_sector)

            if preprocess(row.get('Suboutcome')) is not None:
                suboutcome = onto.SubOutcome(preprocess(row.get('Suboutcome')))
                suboutcome.has_literal = [preprocess(row.get('Suboutcome'), True)]
                outcome.has_suboutcome.append(suboutcome)
                outcome = suboutcome
            indicator = onto.Indicator(preprocess(row.get('Indicator')))
            indicator.has_literal = [preprocess(row.get('Indicator'), True)]
            outcome.has_indicator.append(indicator)
            project.has_indicator.append(indicator)

            if preprocess(row.get('Proxy')) is not None:
                proxy = onto.Proxy(preprocess(row.get('Proxy')))
                proxy.has_literal = [preprocess(row.get('Proxy'), True)]
                indicator.has_proxy.append(proxy)
                if preprocess(row.get('ProxySource')) is not None:
                    proxy_source = onto.Source(preprocess(row.get('ProxySource')))
                    proxy_source.has_literal = [preprocess(row.get('ProxySource'), True)]
                    proxy.has_source.append(proxy_source)

            if preprocess(row.get('IndicatorSource')) is not None:
                source = onto.Source(preprocess(row.get('IndicatorSource')))
                source.has_literal = [preprocess(row.get('IndicatorSource'), True)]
                indicator.has_source.append(source)

            if preprocess(row.get('Stakeholder')) is not None:
                stakeholder = onto.Stakeholder(preprocess(row.get('Stakeholder')))
                stakeholder.has_literal = [preprocess(row.get('Stakeholder'), True)]
                indicator.has_receiver.append(stakeholder)

            if preprocess(row.get('SocialDimension')) is not None:
                dimension = onto.SocialDimension(True)
                indicator.has_dimension.append(dimension)

            if preprocess(row.get('EnvironmentalDimension')) is not None:
                dimension = onto.EnvironmentalDimension(True)
                indicator.has_dimension.append(dimension)

            if preprocess(row.get('EconomicDimension')) is not None:
                dimension = onto.EconomicDimension(True)
                indicator.has_dimension.append(dimension)


def save_ontology(onto, file_name=FILE_NAME):
    onto.save(file_name + '.xml', 'rdfxml')
    onto.save(file_name, 'ntriples')


def preprocess(individual, spaces=False):
    if type(individual) is not str:
        return None

    trimmed = ' '.join(individual.split())
    alphanumeric = [character.lower() for character in trimmed if character.isalnum() or character in [' ', ',', '.', '-', '%', '(', ')']]
    individual = re.sub(r'(?<!^)(?=[A-Z])', '_', ''.join(alphanumeric))

    if not spaces:
        individual = re.sub(' ', '_', individual)
    return individual


if __name__ == '__main__':
    run()
